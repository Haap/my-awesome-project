using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klasser__del_2_
{
    class Sång
    {
        private string artist;
        public string Artist
        {
            get { return artist; }
            set { artist = value; }
        }
        private string sångnamn;
        public string Sångnamn
        {
            get { return sångnamn; }
            set { sångnamn = value; }
        }
        private int speltid;
        public int Speltid
        {
            get { return speltid; }
            set { speltid = value; }
        }
        public Sång(string mottagenArtist, string mottagenSångnamn, int mottagenSpeltid)
        {
            artist = mottagenArtist;
            sångnamn = mottagenSångnamn;
            speltid = mottagenSpeltid;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Sång förstaSången = new Sång("Magnus Uggla", "Kung för en dag", 203);
            Console.WriteLine(förstaSången.Artist + " - " + förstaSången.Sångnamn + " (" + förstaSången.Speltid + "s)");
        }
    }
}
