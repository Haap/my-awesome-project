﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vektorer_och_Sökning
{
    class Program
    {
        static void Main(string[] args)
        {  
            bool isRunning = true;
            string[] array = new string[5];


            while (isRunning)
            {
                Console.WriteLine("[1] Lägg till 5 föremål");
                Console.WriteLine("[2] Skriv ut alla föremål");
                Console.WriteLine("[3] Sök i ryggsäcken");
                Console.Write("[4] Rensa föremålen \nVälj: ");

                bool lyckad = Int32.TryParse(Console.ReadLine(), out int siffra); 
                if (!lyckad)
                { 
                    Console.WriteLine("Du måste skriva in 1, 2 eller 3.");
                }

                switch (siffra)
                {
                    case 1:
                        for (int i = 0; i < array.Length; i++)
                        {
                            if (array[i] == null)
                            {
                                array[i] = Console.ReadLine();
                            }
                            else
                            {
                                Console.WriteLine("Du måste rensa dina föremål innan du skriver in på nytt igen!\n");
                                break;
                            }
                        }
                        break;
                        
                    case 2:
                        for (int i = 0; i < array.Length; i++)
                        {
                            if(array[i] == null)
                            {
                                Console.WriteLine("Du har inte skrivit ut något föremål ännu.\n");
                                break;
                            }
                            else
                            {
                                Console.WriteLine(array[i]);
                            } 
                        }
                        break;                                            
                        
                    case 3:
                        string keyword = Console.ReadLine();
                        for (int i = 0; i < array.Length; i++)
                        {
                            if(array[i] == null)
                            {
                                Console.WriteLine("Du måste skriva in ett föremål innan du söker!\n");
                                break;
                            }

                            else if (array[i].ToUpper() == keyword.ToUpper())
                            {
                                Console.WriteLine("Ordet " + keyword + " finns med i listan!");
                                break;
                            }

                            else if (i == 4)
                            {
                                Console.WriteLine("Ordet " + keyword + " finns tyvärr inte med i listan.");
                            }
                        }                            
                        break;

                    case 4:
                        {
                            Console.WriteLine("\n================\nRensar undan...\n25%\n50%\n75%\n100%\n================\n");
                            Array.Clear(array, 0, array.Length);
                            break;
                        }
                }
            }
        }
    }
}
