﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bloggen
{
    class Program
    {
        static void Main(string[] args)
        {

            List<string[]> minBlogg = new List<string[]>();

            bool isRunning = true;

            while(isRunning)
            {
                Console.WriteLine("[1] - Skriv ett nytt inlägg");
                Console.WriteLine("[2] - Sök inlägg");
                Console.WriteLine("[3] - Skriv ut alla blogginlägg");
                Console.WriteLine("[4] - Avsluta programmet");

                string[] inlägg = new string[2];

                Int32.TryParse(Console.ReadLine(), out int input);
                switch (input)
                {
                    case 1:
                        Console.Write("Ange titel: ");
                        inlägg[0] = Console.ReadLine();
                        Console.Write("Ange beskrivning: ");
                        inlägg[1] = Console.ReadLine();
                        Console.WriteLine("Vill du spara? Ja/Nej");
                        string spara = Console.ReadLine();

                        if(spara.ToUpper() == "Ja".ToUpper())
                        {
                            minBlogg.Add(inlägg);
                            Console.WriteLine("Inlägget sparat!\n");
                        }
                        else if (spara.ToUpper() == "Nej".ToUpper())
                        {
                            Console.WriteLine("Sparas ej.\n");
                        }
                        else
                        {
                            Console.WriteLine("Du valde inget utav alternativen, inlägget sparas ej.");
                        }
                        break;

                    case 2:
                        Console.WriteLine("Skriv inlägget du vill söka upp (namnet på titeln): ");
                        string keyword = Console.ReadLine();
                        for(int i = 0; i < minBlogg.Count; i++)
                        if(keyword == minBlogg[i][0])
                        {
                                Console.WriteLine("Din titel: " + minBlogg[i][0]);
                                Console.WriteLine("Din beskrivning: " + minBlogg[i][1]);
                        }
                        break;
                    case 3:
                        for(int i = 0; i < minBlogg.Count; i++)
                        {
                            Console.WriteLine("\nTitel" + " " + (i+1) + ": " + minBlogg[i][0]);
                            Console.WriteLine("Beskrivning: " + minBlogg[i][1]);
                        }
                        break;
                    case 4:
                        Console.WriteLine("Du valde att avsluta programmet.");
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Du måste ange 1 - 4 för att gå vidare.");
                        break;
                }    
            }
        }
    }
}
